const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')

describe('JobPosting', function() {
    describe('checkEnableTime', function () {
        it('shoud return true when เวลาสมัครอยู่ในช่วงเวลาระหว่างเริ่มต้นและเวลาสิ้นสุด', function() {
            // Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 3)
            const expectedResult = true;
            // ACt
            const actualResult = checkEnableTime(startTime, endTime, today)
    
            // Assert
            assert.strictEqual(actualResult, expectedResult)
        })
        it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
            // Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 1, 30)
            const expectedResult = false;
            // ACt
            const actualResult = checkEnableTime(startTime, endTime, today)
    
            // Assert
            assert.strictEqual(actualResult, expectedResult)
        })
        it('shoud return false when เวลาสมัครอยู่หลังเวลาเริ่มสิ้นสุด', function () {
            // Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 6)
            const expectedResult = false;
            // ACt
            const actualResult = checkEnableTime(startTime, endTime, today)
    
            // Assert
            assert.strictEqual(actualResult, expectedResult)
        })
        it('shoud return true when เวลาสมัครอยู่เท่ากับเวลาเริ่มต้น', function () {
            // Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 1, 31)
            const expectedResult = true;
            // ACt
            const actualResult = checkEnableTime(startTime, endTime, today)
    
            // Assert
            assert.strictEqual(actualResult, expectedResult)
        })
        it('shoud return true when เวลาสมัครอยู่เท่ากับเวลาสิ้นสุด', function () {
            // Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 5)
            const expectedResult = true;
            // ACt
            const actualResult = checkEnableTime(startTime, endTime, today)
    
            // Assert
            assert.strictEqual(actualResult, expectedResult)
        })
    })
    
})
